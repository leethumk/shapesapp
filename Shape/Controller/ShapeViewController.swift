//
//  ViewController.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import UIKit

class ShapeViewController: UIViewController {
    
   let dataHandler = DataHandler()
   private struct Constants {
    static let minRadius:Float = 10
    static let maxRadius:Float = 100
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(singleTapAction(_:)))
        singleTap.numberOfTouchesRequired = 1
        view.addGestureRecognizer(singleTap)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(doubleTapAction(_:)))
        doubleTap.numberOfTapsRequired = 2
        view.addGestureRecognizer(doubleTap)
        singleTap.require(toFail: doubleTap)
    }
    
    // MARK:- Gesture Handlers
    
    @objc func doubleTapAction(_ sender:UITapGestureRecognizer)
    {
        guard sender.view != nil else { return }
        let tappedPosition = sender.location(in: self.view)
        guard let sublayers = view.layer.sublayers else { return }
        sublayers.forEach { layer in
            if let circle = layer as? Circle, let path = circle.path, path.contains(tappedPosition) {
                fillCircleWithColor(circle: circle)
            }
        }
    }
    
    @objc func singleTapAction(_ sender:UITapGestureRecognizer)
    {
        guard sender.view != nil else { return }
        let tappedPosition = sender.location(in:view)
        let randomRadius:Float = Float.random(Constants.minRadius..<Constants.maxRadius)
        let circleShapeLayer =  getCircleIn(position: tappedPosition, radius: randomRadius)
        fillCircleWithColor(circle: circleShapeLayer)
        view.layer.addSublayer(circleShapeLayer)
    }
    
    // MARK:- Circle Shape functions
    
    func getCircleIn(position:CGPoint,radius: Float) -> Circle
    {
        let circle = Circle(with: position, radius: CGFloat(radius))
        addAnimationTo(layer: circle)
        return circle
    }
    
    func fillCircleWithColor(circle:Circle)
    {
        dataHandler.getCircleColor(){ result in
            if let color = result.color
            {
                circle.fillColor = color.toUIColor().cgColor
                circle.strokeColor = color.toUIColor().cgColor
            }
            else
            {
                circle.fillColor = UIColor.randomColor().cgColor
                circle.strokeColor = UIColor.randomColor().cgColor
            }
        }
    }
    
    // MARK:- Animate Shape
    
    func addAnimationTo(layer:CAShapeLayer)
    {
        layer.lineWidth = 0.0
        let lineWidthAnimation = CABasicAnimation(keyPath: "lineWidth")
        lineWidthAnimation.toValue = 10.0
        lineWidthAnimation.duration = 0.5
        lineWidthAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        lineWidthAnimation.autoreverses = true
        layer.add(lineWidthAnimation, forKey: "lineWidthAnimation")
    }
    
    // TODO: Square shapeImplementation
    
    func testSquareIn(position:CGPoint)
    {
        let square = getSquareIn(position: position, frame: CGRect.init(x: 0, y: 0, width: 150, height: 150))
        view.layer.addSublayer(square)
    }
    
    func getSquareIn(position:CGPoint,frame:CGRect) -> CAShapeLayer
    {
        let square = Square(with: position, frame: frame)
        // testing pattern image
        if let patternImage = UIImage(named: "pattern")
        {
            square.fillColor = UIColor(patternImage: patternImage).cgColor
        }
        return square
    }
    
    // Remove custom ShapeLayer
    
    func removeCustomShapeLayers() {
        guard let sublayers = view.layer.sublayers as? [Circle] else { return }
        sublayers.forEach { layer in
            layer.removeAllAnimations()
            layer.removeFromSuperlayer()
        }
    }
    
    
}




