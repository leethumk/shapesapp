//
//  CGFloat+Util.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import UIKit

extension CGFloat {
    static func random() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
}
