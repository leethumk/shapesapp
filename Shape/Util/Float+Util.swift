//
//  Float+Util.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import Foundation

extension Float {
   static func random(_ range:Range<Float>) -> Float
    {
        return Float(range.lowerBound) + Float(arc4random_uniform(UInt32(range.upperBound - range.lowerBound)))
    }
}
