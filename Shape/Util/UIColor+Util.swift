//
//  UIColor+Util.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import UIKit

extension UIColor {
    static func randomColor() -> UIColor {
        
        return UIColor(red:.random(),
                       green:.random(),
                       blue: .random(),
                       alpha: 1.0)
    }
}
