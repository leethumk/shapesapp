//
//  Service.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get
    case post
}

enum ServiceError{
    case serverError
    case parseError
    case urlError
}

enum Result<Value,Error>{
    case success(Value)
    case failure(Error)
}

typealias ServiceCompletion = (Result< Data, ServiceError>)->Void

class Service {
    
    private enum Constants{
        static let baseUrl = "http://www.colourlovers.com"
    }
    
    func sendRequest(with endPoint:String = "",
                         method :HTTPMethod = .get,
                         headers : [String:String]? = nil,
                         parameters:[String:String]? = nil,
                         body:Data? = nil,
                         completion:@escaping ServiceCompletion)
    {
        var urlString = Constants.baseUrl + endPoint
        
        if method == .get , let parameters = parameters {
                urlString = urlString + parameterString(with:parameters)
        }
        guard let url = URL(string: urlString) else {
            return completion(.failure(.urlError))
        }
        var request = URLRequest(url: url)
        if  method == .post,
            let headers = headers,
            let body = body
            {
            request.allHTTPHeaderFields = headers
            request.httpMethod = method.rawValue
            request.httpBody = body
        }
        let dataRequest = URLSession.shared.dataTask(with: request) { data, response, error in
            DispatchQueue.main.async {
                guard let data = data, error == nil else
                {
                    return completion(.failure(.serverError))
                }
                completion(.success(data))
            }
        }
        dataRequest.resume()
    }

    func parameterString(with parameters:[String:String])->String
    {
        var parameterString:String = "?"
        parameters.forEach {
            parameterString = parameterString + $0.0 + "=" + $0.1 + "&"
        }
        return String(parameterString.dropLast())
    }
}
