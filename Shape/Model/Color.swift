//
//  Color.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//
import UIKit

struct Color:Codable{
    let red:Int
    let green:Int
    let blue:Int
    func toUIColor()-> UIColor  {
        return UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1)
    }
}
struct ColorInfo:Codable{
    let rgb:Color
}

