//
//  DataHandler.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import Foundation
import UIKit

typealias ColorResult = (color :Color?,error:String?)
typealias PatternResult = (image:UIImage?,error:String?)

typealias ColorCompletionBlock = (ColorResult)->Void
typealias PatternCompletionBlock = (PatternResult)->Void

class DataHandler{
    
    private enum Constants {
        static let colorEndPoint = "/api/colors/random?format=json"
        static let patternEndPoint = "/api/patterns/random?format=json"
        static let parseErrorMessage = "Error Parsing the data"
        static let serverErrorMessage = "Error fetching data from server"
        static let urlErrorMessage = "Error with Url"
    }
    let service = Service ()
    
    func getCircleColor(completion:@escaping ColorCompletionBlock )
    {
        var returnValue:ColorResult
        service.sendRequest(with: Constants.colorEndPoint ,method:.get){  [weak self] result in
            guard let this = self else { return }
            switch result {
            case .failure(let error):
                returnValue.error = this.getErrorDescription(error: error)
            case .success(let data):
                // let color = Color(red: 111, green: 200, blue: 114)
                //returnValue.color = color
                returnValue = this.readColorFrom(data: data)
            }
            completion(returnValue)
        }
    }
    
    func readColorFrom(data:Data) -> ColorResult
    {
        var color:Color? = nil
        var parseError:String? = nil
        
        do{
            let parsedArray = try JSONDecoder().decode([ColorInfo].self, from: data)
            color = parsedArray.last?.rgb
        }
        catch{
            parseError = getErrorDescription(error:.parseError)
        }
        return (color,parseError)
    }
    
    func getErrorDescription(error:ServiceError)->String
    {
        var errorMessage = ""
        switch error {
        case .parseError:
            errorMessage = Constants.parseErrorMessage
        case .serverError:
            errorMessage = Constants.serverErrorMessage
        case .urlError:
            errorMessage = Constants.urlErrorMessage
        }
        return errorMessage
    }
    
    // MARK:- ToDo Square pattern Service
    
    func getSquarePattern(completion:@escaping PatternCompletionBlock ) {
        
        var returnValue:PatternResult
        service.sendRequest(with: Constants.patternEndPoint ,method:.get){  [weak self] result in
            guard let this = self else { return }
            switch result {
            case .failure(let error):
                returnValue.error = this.getErrorDescription(error: error)
            case .success(let data):
                returnValue.image = UIImage(data: data)
            }
            completion(returnValue)
        }
    }
    
}
