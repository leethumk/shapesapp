//
//  Square.swift
//  Shape
//
//  Created by Leethu Sanal on 25/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import UIKit

class Square: CAShapeLayer {
    
    init(with path:CGPath)
    {
        super.init()
        self.path = path
        self.fillColor = UIColor.black.cgColor
        self.strokeColor = UIColor.black.cgColor
    }
    
    init(with position:CGPoint,frame:CGRect)
    {
        super.init()
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: frame.size.height))
        path.addLine(to: CGPoint(x: frame.size.width, y:frame.size.height))
        path.addLine(to: CGPoint(x: frame.size.width, y: 0.0))
        path.close()
        self.path = path.cgPath
        self.position = position
        
    }
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
