//
//  Circle.swift
//  Shape
//
//  Created by Leethu Sanal on 24/7/18.
//  Copyright © 2018 Leethu Sanal. All rights reserved.
//

import UIKit

class Circle: CAShapeLayer {
    
    init(with path:CGPath)
    {
        super.init()
        self.path = path
        self.fillColor = UIColor.black.cgColor
        self.strokeColor = UIColor.black.cgColor
    }
    init(with position:CGPoint,radius:CGFloat)
    {
        super.init()
        let path = UIBezierPath(arcCenter: position, radius:CGFloat(radius), startAngle: 0, endAngle: 360, clockwise: true).cgPath
        self.path = path
        self.fillColor = UIColor.black.cgColor
        self.strokeColor = UIColor.black.cgColor
        
    }
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
